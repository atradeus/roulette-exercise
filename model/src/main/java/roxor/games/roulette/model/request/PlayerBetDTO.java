package roxor.games.roulette.model.request;

public record PlayerBetDTO(String playerName, String betType, String pocket, String betAmount) {

}
