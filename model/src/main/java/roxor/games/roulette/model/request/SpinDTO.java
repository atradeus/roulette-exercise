package roxor.games.roulette.model.request;

import java.util.List;

public record SpinDTO(String forcedPocket, List<PlayerBetDTO> playerBets) {

}
