package roxor.games.roulette.model.response;

import jakarta.validation.constraints.NotEmpty;
import roxor.games.roulette.model.request.PlayerBetDTO;

public record BetResultDTO(@NotEmpty PlayerBetDTO playerBet, @NotEmpty String outcome, @NotEmpty String winAmount) {
}
