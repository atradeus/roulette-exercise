package roxor.games.roulette.model.response;

import jakarta.validation.constraints.NotEmpty;

import java.util.List;

public record SpinResultDTO(@NotEmpty Integer pocket, @NotEmpty List<BetResultDTO> betResults) {

}
