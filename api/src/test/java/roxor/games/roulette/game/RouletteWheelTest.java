package roxor.games.roulette.game;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RouletteWheelTest {

    private RouletteWheel rouletteWheel = new RouletteWheel();



    @Test
    void canForcePocketNumber() {
        int forcedPocketNumber = 0;
        WheelResult result = rouletteWheel.spin(forcedPocketNumber);
        assertEquals(forcedPocketNumber, result.pocketResult());
    }

    @Test
    void pocketResultFallsWithinExpectedBounds() {
        int spins = 1000;
        for (int i = 0; i < spins; i++) {
            int actualPocketNumber = rouletteWheel.spin(null).pocketResult();
            assertTrue(0 <= actualPocketNumber && actualPocketNumber <= 36,
                    "Expected actualPocketNumber to be within the range [0,36] but was: " + actualPocketNumber);
        }
    }



}