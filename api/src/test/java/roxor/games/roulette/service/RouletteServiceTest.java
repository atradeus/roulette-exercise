package roxor.games.roulette.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import roxor.games.roulette.controller.exception.BadBetException;
import roxor.games.roulette.game.RouletteWheel;
import roxor.games.roulette.model.request.PlayerBetDTO;
import roxor.games.roulette.model.request.SpinDTO;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Anthony Merlo
 * @since 04/06/2024
 */
class RouletteServiceTest {

    private RouletteService rouletteService;

    @BeforeEach
    void setUp() {
        rouletteService = new RouletteService(new RouletteWheel());
    }

    @Test
    void spinWin() {
        var result = rouletteService.spin(new SpinDTO("1", List.of(playerBet())));
        assertNotNull(result);
        assertFalse(result.betResults().isEmpty());
        var bet = result.betResults().get(0);
        assertEquals(bet.outcome(), "win");
        assertEquals(bet.winAmount(), "36.0");
    }

    @Test
    void spinLose() {
        var result = rouletteService.spin(new SpinDTO("2", List.of(playerBet())));
        assertNotNull(result);
        assertFalse(result.betResults().isEmpty());
        var bet = result.betResults().get(0);
        assertEquals(bet.outcome(), "lose");
        assertEquals(bet.winAmount(), "0.0");
    }

    @Test
    void invalidBets() {
        assertThrows(BadBetException.class, () -> rouletteService.spin(null));
        assertThrows(BadBetException.class, () -> rouletteService.spin(new SpinDTO("1", null)));
    }

    private static PlayerBetDTO playerBet() {
        return new PlayerBetDTO("name", "PocketBet", "1", "1");
    }
}