package roxor.games.roulette.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import roxor.games.roulette.model.request.SpinDTO;
import roxor.games.roulette.model.response.SpinResultDTO;
import roxor.games.roulette.service.RouletteService;

@RestController
@RequestMapping("/v1/roulette")
public class RouletteController {

    private final Logger logger = LoggerFactory.getLogger(RouletteController.class);

    private final RouletteService rouletteService;

    RouletteController(RouletteService rouletteService) {
        this.rouletteService = rouletteService;
    }

    @PostMapping("/spin")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<SpinResultDTO> spin(@RequestBody SpinDTO spinDTO) {
        logger.info("SpinDTO: " + spinDTO);

        SpinResultDTO spinResultDTO;
        try {
            spinResultDTO = rouletteService.spin(spinDTO);
        } catch (Exception e) {
            //logger.error("Roulette error {}", e);
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        logger.info("SpinResultDTO: " + spinResultDTO);

        return ResponseEntity.ok(spinResultDTO);
    }
}
