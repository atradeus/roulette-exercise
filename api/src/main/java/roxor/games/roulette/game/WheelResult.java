package roxor.games.roulette.game;

public record WheelResult(int pocketResult) {

    public static WheelResult create(int pocketResult) {
        return new WheelResult(pocketResult);
    }
}
