package roxor.games.roulette.service;

import org.springframework.stereotype.Component;
import roxor.games.roulette.game.RouletteWheel;
import roxor.games.roulette.model.request.SpinDTO;
import roxor.games.roulette.model.response.BetResultDTO;
import roxor.games.roulette.model.response.SpinResultDTO;

@Component
public class RouletteService {

    public enum BetTypes {PocketBet};

    private static final Double WIN_MULTIPLIER = 36.;
    private final RouletteWheel rouletteWheel;

    RouletteService(RouletteWheel rouletteWheel) {
        this.rouletteWheel = rouletteWheel;
    }

    public SpinResultDTO spin(SpinDTO spinDTO) {
        // Validate input
        ValidationService.validate(spinDTO);

        Integer forcedPocket = spinDTO.forcedPocket() != null && !spinDTO.forcedPocket().isEmpty()
                ? Integer.parseInt(spinDTO.forcedPocket()) : null;

        final String result = Integer.toString(rouletteWheel.spin(forcedPocket).pocketResult());

        return new SpinResultDTO(forcedPocket,
                spinDTO.playerBets()
                        .stream()
                        .map(p -> {
                            double winAmount = 0.;
                            String outcome = "lose";

                            switch (BetTypes.valueOf(p.betType())) {
                                case PocketBet -> {
                                    if (result.equals(p.pocket())) {
                                        winAmount = WIN_MULTIPLIER * Double.parseDouble(p.betAmount());
                                        outcome = "win";
                                    }
                                }
                            }

                            return new BetResultDTO(p, outcome, Double.toString(winAmount));
                        })
                        .toList());
    }

}
