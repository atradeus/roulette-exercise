package roxor.games.roulette.service;

import roxor.games.roulette.controller.exception.BadBetException;
import roxor.games.roulette.model.request.PlayerBetDTO;
import roxor.games.roulette.model.request.SpinDTO;

/**
 * @author Anthony Merlo
 * @since 03/06/2024
 */
public class ValidationService {


    public static void validate(SpinDTO spinDTO) throws BadBetException {
        if (spinDTO == null || spinDTO.playerBets() == null || spinDTO.playerBets().isEmpty()) {
            throw new BadBetException("No player bets");
        }

        // Validate player bets
        for (PlayerBetDTO bet : spinDTO.playerBets()) {
            if (bet == null
                    || bet.playerName() == null
                    || bet.playerName().isEmpty()
                    || bet.betType() == null
                    || bet.betType().isEmpty()
            ) {
                throw new BadBetException("Invalid player bet");
            }

            try {
                RouletteService.BetTypes.valueOf(bet.betType());
            } catch (IllegalArgumentException e) {
                throw new BadBetException("Invalid bet type");
            }

            testNumber(bet.pocket());
            testNumber(bet.betAmount());
        }
    }

    private static void testNumber(String value) {
        double number = 0;
        try {
            number = Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new BadBetException("Invalid number " + value);
        }

        if (number <= 0) {
            throw new BadBetException("Value must be a positive number greater than zero");
        }
    }
}
